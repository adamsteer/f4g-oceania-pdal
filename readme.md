# Processing and visualising point clouds with PDAL, Entwine Point Tiles and QGIS

**Dr. Adam Steer**  
https://spatialised.net  
@adamdsteer  
adam@spatialised.net

## Warning

A lot has changed in PDAL since 2018, and I no longer use Amazon Web Services to host data. Some parts of this workshop do not work as advertised until data can be migrated to a new host. Others may work better with newer PDAL features. An update is slowly arriving...

## Introduction

This repository contains materials for a point cloud processing workshop developed by Dr. Adam Steer, originally for the FOSS4G SotM Oceania conference in 2018.

It uses the [Point Data Abstraction Library](http://pdal.io) (PDAL), [Entwine](http://entwine.io) Python, Numpy, Jupyter notebooks, and [QGIS](http://qgis.org) to develop some concepts about processing point clouds and visualising results.

![PDAL logo](https://pdal.io/_images/pdal_logo.png)  
![Entwine logo](https://github.com/connormanning/entwine/blob/master/doc/logo/color/entwine_logo_2-color-small.png)

## What this workshop will deliver

This workshop uses common point cloud data analysis tasks to demonstrate 'thinking in PDAL'. Using both lidar and photogrammetric data, users are shown how to apply a range of simple strategies to construct complex workflows. These are demonstrated using a command line interface, then using configuration files to hold common parameters, and finally using Python for running processes and exploratory visualisation.

It walks through data visualisation in QGIS for some tasks, showing how to use QGIS as a point cloud viewer and a tool for using derived products.

At the end of the workshop users should have a greater understanding of how PDAL's pipeline architecture works, and some ideas about how to apply it in their own data analysis tasks.

## Why this workshop exists alongside a PDAL workshop maintained by the PDAL team

PDAL's authors maintain a workshop here: https://pdal.io/workshop/index.html . It is a fantastic tour of PDAL's diverse capabilities, and you should take that workshop as well! The reason I wrote this workshop was to focus on some end to end workshop examples. This workshop *leaves out* a huge amount of PDAL capability in order to:
- focus on some simple, commonly-used patterns.
- reiterate the PDAL approach in a different way to how it is explained in the main PDAL workshop, building from commands to pipelines and simple Python integration

I see this as completely complementary to the main workshop - which should keep its character as a *tour de force* of PDAL, I hope you do also.

## Materials

Text relating to the workshop is contained in [workshop](./workshop). Jupyter notebooks used in the workshop are contained in [notebooks](./notebooks). Sample workflows as PDAL pipelines are contained in [resources](./resources).

## Preparation

This workshop requires a PDAL installation with python bindings. Using the minified [Conda package manager](https://docs.conda.io/en/latest/miniconda.html) is convenient and works across Linux, MacOS and Windows. A native installation should work just fine as well. However PDAL is installed, it needs to be at least release 2.1, and be visible to a Jupyter notebook.

Using Conda, create a new virtual environment like:

`conda create -n f4g-pdal-workshop python pdal python-pdal entwine jq numpy pandas matplotlib jupyter scipy seaborn -c conda-forge`

This creates a virtual environment with PDAL, PDAL Python bindings, and Entwine installed, using the `conda-forge` channel. It also installs the `jq` command line utility, for parsing PDAL's metadata responses. Numpy, Pandas, Matplotlib and Jupyter are all Python packages for data manipulation and visualisation.

To use the new environment head to a command line interface, switch to the exercises directory in this repository and type:

`conda activate f4g-pdal-workshop`

..and then:

`jupyter notebook`

...to get access to the processing notebooks.

To visualise some data products, please ensure you have:
- [QGIS](http://qgis.org) or an alternate viewer for geoJSON and georeferenced raster data

...especially if you're not using Jupyter notebooks to run tasks. QGIS can be installed on a per-user basis (no admin permissions required) using the Conda package manager.

### Sample data

You should not need to download data ahead of this workshop. You will need a good data connection.

The following publicly available Entwine Point Tile and Cloud Optimized Point Cloud (COPC) datasets are used:


If you want to run this workshop on locally held point clouds, reach out for pricing.

## Important changes

This workshop no longer uses dockerised PDAL, choosing instead to focus on capabilities present in Conda / Mamba packaged builds for cross-platform utility. If you need a bleeding edge feature or native PDAL installation, reach out.

## Usage

Feel free to explore and use the workshop materials as you see fit, acknowledgment is appreciated. All data samples used in this workshop are licensed CCBY4 and available in public repositories.

I'd also be very happy to run this workshop and customised variations of it for your organisation - please contact me for details and pricing.
